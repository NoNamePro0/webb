package main

import (
	"bytes"
	"fmt"
	"net"
	"os"
	"strings"

	"github.com/valyala/fasthttp"
)

func main() {
	port := envOr("PORT", "80")
	host := envOr("HOST", "[::]")
	fmt.Println("Listening on http://" + host + ":" + port)
	enableHidden := envFlag("ENABLE_HIDDEN", true)
	enableCache := envFlag("ENABLE_CACHE", true)
	notFound := "/" + strings.TrimPrefix(envOr("NOT_FOUND", "404.html"), "/")
	fsHandler := (&fasthttp.FS{
		Root:               ".",
		GenerateIndexPages: envFlag("ENABLE_INDEX", false),
		Compress:           envFlag("ENABLE_COMPRESSION", false),
		AcceptByteRange:    envFlag("ENABLE_RANGE", true),
		IndexNames:         strings.Split(envOr("INDEX_FILES", "index.html;index.htm"), ";"),
		PathNotFound: func(ctx *fasthttp.RequestCtx) {
			ctx.Response.SetStatusCode(404)
		},
	}).NewRequestHandler()
	listener, err := net.Listen("tcp", host+":"+port)
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
	if err := (&fasthttp.Server{
		GetOnly: true,
		Logger:  enabledLogger(envFlag("DEBUG", false)),
		Handler: func(ctx *fasthttp.RequestCtx) {
			if !enableHidden && bytes.Contains(ctx.Path(), []byte{'/', '.'}) { // block hidden files
				ctx.Logger().Printf("dotfile access is disabled")
				ctx.Response.SetStatusCode(403)
			} else { // let fasthttp handle the request
				fsHandler(ctx)
			}
			if ctx.Response.StatusCode() > 399 { // serve not found page for all request/server errors
				if notFound != "/" { // try NOT_FOUND first if set
					ctx.Request.Reset()
					ctx.Response.Reset()
					ctx.Request.SetRequestURI(notFound)
					fsHandler(ctx) // we can't use ServeFile as it doesn't respect compression/indexing options
				} else { // fall back to a plain text response if that failed
					ctx.Error("File not found", 404)
				}
				ctx.SetStatusCode(404)
				ctx.Response.Header.Set("Cache-Control", "no-cache") // That on reload the brower rechecks, if file available.
			}

			// set security headers & Server header
			ctx.Response.Header.Set("Referrer-Policy", "strict-origin-when-cross-origin")
			ctx.Response.Header.Set("X-Frame-Options", "SAMEORIGIN")
			ctx.Response.Header.Set("X-XSS-Protection", "1; mode=block")
			ctx.Response.Header.Set("Server", "webb")
			if !enableCache {
				ctx.Response.Header.Set("Cache-Control", "no-cache")
			}
		},
	}).Serve(listener); err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
}

type enabledLogger bool // debug logger that can be disabled by setting it to false
func (l enabledLogger) Printf(format string, args ...interface{}) {
	if l {
		fmt.Printf(format+"\n", args...)
	}
}

// helpers for environment variables
func envOr(env string, or string) string {
	if v := os.Getenv(env); v != "" {
		return v
	}
	return or
}
func envFlag(env string, or bool) bool {
	if !or {
		return strings.ContainsAny(os.Getenv(env), "YyTt1") // default: false, only return true if intended
	}
	return !strings.ContainsAny(os.Getenv(env), "NnFf0") // default: true, only return false if intended
}
