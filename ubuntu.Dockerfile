FROM golang:alpine as builder
RUN apk add --no-cache \
    git \
    upx
WORKDIR /build/webb
ADD . .
RUN CGO_ENABLED=0 go build -a -ldflags '-s -w -extldflags "-static"' .
RUN upx webb
RUN mkdir -p /output/var/www /output/bin ; \
    cp webb /output/bin/ ; \
    cp *.html /output/var/www/
FROM ubuntu
COPY --from=builder /output/ /
WORKDIR /var/www
EXPOSE 80
VOLUME ["/var/www"]
ENTRYPOINT ["/bin/webb"]
