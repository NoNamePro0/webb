build:
	go build

docker:
	docker build . -t nonamepro/webb        -f scratch.Dockerfile

docker-alpine:
	docker build . -t nonamepro/webb:alpine -f alpine.Dockerfile

docker-debian:
	docker build . -t nonamepro/webb:debian -f debian.Dockerfile

docker-ubuntu:
	docker build . -t nonamepro/webb:ubuntu -f ubuntu.Dockerfile
