# webb: just serve static files.


Just another fast static fileserver listening on http, which is coded in [Go](https://golang.org/) with the [FastHTTP framework](https://github.com/valyala/fasthttp). Use that for your static html files, like builded [Hugo](https://gohugo.io/) or [jekyll](https://jekyllrb.com/).


## Run
Hosting the current directory on port `80`:
```bash
sudo docker run --rm -it -p 80:80 -v "$PWD:/var/www:ro" nonamepro/webb
```

### Docker Compose
Run via docker-compose:
```yaml
version: "3.8"
services:
  webb:
    image: nonamepro/webb
    ports:
      - "8080:80"
    volumes:
      - ./mywebsite:/var/www
```

[![Try in PWD](https://github.com/play-with-docker/stacks/raw/cff22438cb4195ace27f9b15784bbb497047afa7/assets/images/button.png)](http://play-with-docker.com?stack=https://codeberg.org/NoNamePro0/webb/raw/branch/main/docker-compose.yml)

## Base Image
You can use this image as base image, this image is very small and based on scratch
```Dockerfile
FROM nonamepro/webb
COPY mywebsite /var/www
```
You may also looking for these tags: [`alpine`](alpine.Dockerfile), [`debian`](debian.Dockerfile) [`ubuntu`](ubuntu.Dockerfile)

## Build from Source
Option 1: Docker Image
```bash
git clone https://codeberg.org/nonamepro0/webb ; cd webb
sudo make docker

# Run
sudo docker run --rm -it -p 8080:80 -v "$PWD:/var/www:ro" webb
```

Option 2: Binary
```bash
git clone https://codeberg.org/nonamepro0/webb ; cd webb
make

# Run
sudo ./webb
```


> This is a fork and revision of [momar/web](https://codeberg.org/momar/web). webb should be even faster and more user friendly.


## Env-Variables

### `NOT_FOUND`
path to the 404 error page
> Default `/404.html`


### `INDEX_FILES`
semicolon-separated list of files that should be served when accessing a directory
> Default: `index.html;index.htm`

### `ENABLE_INDEX`
show a directory index if no index file could be found
> Default: `0`

### `ENABLE_HIDDEN=0`
allow access to files and directories starting with a dot
> Default: `0`

### `ENABLE_COMPRESSION`
enable [compression](https://godoc.org/github.com/valyala/fasthttp#FS) - if the directory is not mounted read-only (:ro), compressed version of your files will be created with the extension .fasthttp.gz
> Default: `0`

### `ENABLE_RANGE`
enable [HTTP range requests](https://developer.mozilla.org/en-US/docs/Web/HTTP/Range_requests)
> Default: `1`

### `DEBUG`
print all non-OK requests to standard output/docker logs
> Default: `0`

### `HOST`
listening address, doesn't normally have to be changed
> Default: `[::]`

### `PORT`
listening http port for files, on Docker, don't change.
> Default: `80`
